# React Interview

## Description
1. Lister les films dans des cartes avec: le titre en gras, la catégorie et une jauge type Youtube indiquant le ratio likes/dislikes. Les cartes doivent être côtes à côtes et responsive. Càd que lorsque la fenêtre se réduit, les cartes sautent à la ligne suivante.

2. Ajouter un bouton dans les cartes permettant de supprimer celle-ci

3. Ajouter un bouton toggle like/dislike

4. Ajouter un filtre par catégorie (de type multiselect) en supposant qu'on ne les connaisse pas à l'avance (il faut donc les récupérer dynamiquement depuis les films). Si tous les films d'une catégorie sont supprimés, celle-ci ne doit plus apparaître.

5. Ajouter un système de pagination avec les fonctionnalités suivantes:

    - Boutons précédent/suivant
    - Choix du nombre d'élements affichés par page (4, 8 ou 12).

Prenez des initiatives, il y a des points bonus si

- C'est joli
- Vous utilisez correctement REDUX
- Il y a une attention aux détails
/!\ La suppression du comportement asynchrone dans movies.js entraînera une annulation du test.
## Mes Retours

J'ai beaucoup apprécié l'exercice et j'ai beaucoup aimé le faire.

### Modification de la liste des films

Je me suis permis de modifier la liste des movies en y ajoutant une URL d'image pour rendre le tout plus attrayant.

Ces images ont été prises sur le site [The Movie DB (TMDB)](https://www.themoviedb.org/).

### Design
Pour le design, j'ai utilisé la librairie Next UI.


### Tests
Compte tenu du temps, je n'ai pas pu implémenter de tests unitaires, fonctionnels ou d'intégration. Pour une meilleure qualité de code, j'utilise généralement Jest pour implémenter mes tests techniques et d'intégration.
