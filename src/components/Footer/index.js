import { Text } from "@nextui-org/react";
import React from "react";

function Footer() {
  return (
    <Text
      h5
      css={{
        textGradient: "45deg, $blue600 -20%, $pink600 10%",
        textAlign: "center", 
        margin: "2rem",
      }}
    >
      Made by{" "}
      <a href="https://djopa.fr" style={{ color: "white", textDecoration: "underline" }}>
        Wilfried Djopa Tchatat
      </a>.
    </Text>
  );
}

export default Footer;
