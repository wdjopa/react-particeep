import { Text } from "@nextui-org/react";
import React from "react";

function Header() {
  return <Text h2>The Movie DB</Text>;
}

export default Header;
