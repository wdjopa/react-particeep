import { NextUIProvider, createTheme } from "@nextui-org/react";
import { Provider } from "react-redux";
import Home from "./pages/Home";
import { store } from "./stores";

const darkTheme = createTheme({
  type: "dark",
});

function App() {
  return (
    <NextUIProvider theme={darkTheme}>
      <Provider store={store}>
        <Home />
      </Provider>
    </NextUIProvider>
  );
}

export default App;
